package com.score.senz_health.enums;

public enum IntentType {
    SENZ, ADD_USER, SMS_REQUEST_ACCEPT, SMS_REQUEST_REJECT, SMS_REQUEST_CONFIRM, CONNECTED
}
