package com.score.senz_health.ui;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.score.senz_health.R;
import com.score.senz_health.exceptions.NoUserException;
import com.score.senz_health.interfaces.IFragmentTransitionListener;
import com.score.senz_health.pojo.DrawerItem;
import com.score.senz_health.pojo.SecretUser;
import com.score.senz_health.utils.PreferenceUtils;
import com.score.senzc.pojos.User;

import java.util.ArrayList;
import android.app.Fragment;
import com.score.senz_health.pojo.SecretUser;


public class DrawerActivity extends AppCompatActivity implements View.OnClickListener, IFragmentTransitionListener {


    private SecretUser secretUser;

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private RelativeLayout drawerContainer;
    private ListView drawerListView;
    private ActionBarDrawerToggle actionBarDrawerToggle;

    private ImageView homeView;
    private TextView titleText;
    private TextView homeUserText;

    private RelativeLayout aboutLayout;
    private TextView aboutText;

    private boolean isDoc = false;
    private boolean isLab = false;
    // drawer components+++++++-

    private ArrayList<DrawerItem> drawerItemList;
    private DrawerAdapter drawerAdapter;

    private Typeface typeface;

    private static final String G_TAG = "Global"; // For Android Monitor debug : Global Tag

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_layout);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/GeosansLight.ttf");

        setupToolbar();
        setupActionBar();
        setupDrawer();
        initDrawerList();

        // load initial fragment
        loadInitial();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra("SENDER")) {
            //loadFriends();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setupDrawer() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerContainer = (RelativeLayout) findViewById(R.id.drawer_container);

        final LinearLayout frame = (LinearLayout) findViewById(R.id.content_view);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            @SuppressLint("NewApi")
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float moveFactor = (drawerListView.getWidth() * slideOffset);

                frame.setTranslationX(moveFactor);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        homeUserText = (TextView) findViewById(R.id.home_user_text);
        homeUserText.setTypeface(typeface);
        try {
            User user = PreferenceUtils.getUser(this);
            homeUserText.setText("@" + user.getUsername());
        } catch (NoUserException ex) {
            Log.d("TAG", "No Registered User");
        }

        aboutLayout = (RelativeLayout) findViewById(R.id.about_layout);
        aboutLayout.setOnClickListener(this);

        aboutText = (TextView) findViewById(R.id.about_text);
        aboutText.setTypeface(typeface);
    }

    /**
     * Initialize Drawer list
     */
    private void initDrawerList() {
        //Getting SharedPreferences : Key MyRole
        SharedPreferences sharedPreferences = getSharedPreferences("MyRole", Context.MODE_PRIVATE);
        String Role = sharedPreferences.getString("Role", "No role defined");
        // initialize drawer content
        // need to determine selected item according to the currently selected sensor type
        drawerItemList = new ArrayList();
        

		if(Role.equals("Patient")) {
            // patient navigation bar
            drawerItemList.add(new DrawerItem("My profile", R.drawable.senz_health_logo, R.drawable.senz_health_logo, true));
            drawerItemList.add(new DrawerItem("Doctors", R.drawable.senz_health_logo, R.drawable.senz_health_logo, false));
            drawerItemList.add(new DrawerItem("Laboratories", R.drawable.senz_health_logo, R.drawable.senz_health_logo, false));
            drawerItemList.add(new DrawerItem("Get an Appointment", R.drawable.senz_health_logo, R.drawable.senz_health_logo, false));
            drawerItemList.add(new DrawerItem("Manage Reports", R.drawable.senz_health_logo, R.drawable.senz_health_logo, false));
            drawerItemList.add(new DrawerItem("Prescription & Test Details", R.drawable.senz_health_logo, R.drawable.senz_health_logo, false));

            drawerItemList.add(new DrawerItem("Change Role", R.drawable.senz_health_logo, R.drawable.senz_health_logo, false));

        }else if (Role.equals("Doctor")){ // Doctor navigation bar

            drawerItemList.add(new DrawerItem("My profile", R.drawable.senz_health_logo, R.drawable.senz_health_logo, true));
            drawerItemList.add(new DrawerItem("Time Schedule", R.drawable.senz_health_logo, R.drawable.senz_health_logo, false));
            drawerItemList.add(new DrawerItem("Prescription Details", R.drawable.senz_health_logo, R.drawable.senz_health_logo, false));
            drawerItemList.add(new DrawerItem("Patients", R.drawable.senz_health_logo, R.drawable.senz_health_logo, false));
            drawerItemList.add(new DrawerItem("Request Report", R.drawable.senz_health_logo, R.drawable.senz_health_logo, false));
            drawerItemList.add(new DrawerItem("Change Role", R.drawable.senz_health_logo, R.drawable.senz_health_logo, false));

        }else{

            // Labotary navigation bar
            drawerItemList.add(new DrawerItem("My profile", R.drawable.senz_health_logo, R.drawable.senz_health_logo, true));
            drawerItemList.add(new DrawerItem("Patients", R.drawable.senz_health_logo, R.drawable.senz_health_logo, false));
            drawerItemList.add(new DrawerItem("Manage Reports", R.drawable.senz_health_logo, R.drawable.senz_health_logo, false));
            drawerItemList.add(new DrawerItem("Change Role", R.drawable.senz_health_logo, R.drawable.senz_health_logo, false));

        }

        drawerAdapter = new DrawerAdapter(this, drawerItemList);
        drawerListView = (ListView) findViewById(R.id.drawer);

        if (drawerListView != null)
            drawerListView.setAdapter(drawerAdapter);

        drawerListView.setOnItemClickListener(new DrawerItemClickListener());
    }

    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();

        ActionBar.LayoutParams params = new
                ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT, Gravity.CENTER);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.home_action_bar_layout, null), params);
        actionBar.setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setDisplayShowCustomEnabled(true);

        homeView = (ImageView) findViewById(R.id.home_view);
        homeView.setOnClickListener(this);

        titleText = (TextView) findViewById(R.id.title_text);
        titleText.setTypeface(typeface, Typeface.BOLD);
    }

    @Override
    public void onClick(View v) {
        if (v == homeView) {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(drawerContainer);
            } else {
                drawerLayout.openDrawer(drawerContainer);
            }
        } else if (v == aboutLayout) {
            loadAbout();

        }
    }

    @Override
    public void onTransition(String type) {

        SharedPreferences sharedPreferences = getSharedPreferences("MyRole", Context.MODE_PRIVATE);
        String Role = sharedPreferences.getString("Role", "No role defined");

        if (Role.equals("Patient")){
            if (type.equalsIgnoreCase("My profile")) {
                Log.d(G_TAG, "OnTransition : My profile");
                loadMyProfile();
            } else if (type.equalsIgnoreCase("Doctors")) {
                Log.d(G_TAG, "OnTransition : Doctors");
                loadDoctorList();
            } else if (type.equalsIgnoreCase("Laboratories")) {
                Log.d(G_TAG, "OnTransition : Laboratories");
                loadLaboratoryList();
            } else if (type.equalsIgnoreCase("Get an Appointment")){
                Log.d(G_TAG, "OnTransition : Get an Appointment");
                loadGetAppoinment();
            } else if (type.equalsIgnoreCase("Manage Reports")){
                Log.d(G_TAG, "OnTransition : Manage Reports");
                loadManageReports();
            } else if (type.equalsIgnoreCase("Prescription & Test Details")){
                Log.d(G_TAG, "OnTransition : Prescription & Test Details");
                loadprescriptionDetails();

            } else if (type.equalsIgnoreCase("Change Role")){
                Log.d(G_TAG, "OnTransition : Change Role");
                loadChangeRole();
            }

        }else if (Role.equals("Doctor")){
            if (type.equalsIgnoreCase("My profile")) {
                Log.d(G_TAG, "OnTransition : My Profile");
                loadMyProfile();
            } else if (type.equalsIgnoreCase("Time Schedule")) {
                Log.d(G_TAG, "OnTransition : Time Schedule");
                loadTimeSchedule();
            } else if (type.equalsIgnoreCase("Prescription Details")) {
                Log.d(G_TAG, "OnTransition : Prescription Details");
                loadDocPrescriptionDetails();

            }else if (type.equalsIgnoreCase("Patients")) {
                Log.d(G_TAG, "OnTransition : Patients");
                loadPatientList();
            }else if (type.equalsIgnoreCase("Change Role")){
                Log.d(G_TAG, "OnTransition : Change Role");
                loadChangeRole();
            }
        }else{
            if (type.equalsIgnoreCase("My profile")) {
                loadLaboratoryProfile();

            } else if (type.equalsIgnoreCase("Patients")) {
                loadPatientList();
            } else if (type.equalsIgnoreCase("Change Role")){
                Log.d(G_TAG, "OnTransition : Change Role");
                loadChangeRole();
            }

        }

        if (type.equalsIgnoreCase("Invite to Doctor")) {
            loadInviteDoctor();
        }else if (type.equalsIgnoreCase("Invite to Laboratory")) {
            loadInviteLaboratory();
        }


    }

    /**
     * Drawer click event handler
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // Highlight the selected item, update the title, and close the drawer
            // update selected item and title, then close the drawer
            drawerLayout.closeDrawer(drawerContainer);


            SharedPreferences sharedPreferences = getSharedPreferences("MyRole", Context.MODE_PRIVATE);
            String Role = sharedPreferences.getString("Role", "No role defined");

            if(Role.equals("Patient")){
                if (position == 0) {
                    loadMyProfile();
                }else if (position == 1) {
                    loadDoctorList();
                }else if (position == 2) {
                    loadLaboratoryList();
                }else if (position == 3) {
                    loadGetAppoinment();
                }else if (position == 4) {
                    loadManageReports();
                }else if (position == 5) {
                    loadprescriptionDetails();
                }else if (position == 6) {
                    loadChangeRole();

                }
            }else if (Role.equals("Doctor")){
                if (position == 0) {
                    loadMyProfile();
                }else if (position == 1){
                    loadTimeSchedule();
                }else if (position == 2){
                    loadDocPrescriptionDetails();
                }else if(position == 3){
                    isDoc = true;
                    loadPatientList();
                }else if (position == 4) {
                    loadRequestReport();
                }else if (position == 5){
                    loadChangeRole();
                }
            }else{
                if (position == 0) {
                    loadLaboratoryProfile();
                }else if(position == 1){
                    loadPatientList();
                }else if (position == 2) {
                    isLab = true;
                    loadManageReports();
                }else if(position == 3){
                    loadChangeRole();
                }

            }
        }
    }

    /**
     * Load my sensor list fragment
     */


    /**
     * Load my sensor list fragment
     */


    public void loadInviteDoctor() {
        titleText.setText("Invite to Doctor");
        clearAboutText();

        InviteDoctorFragment fragment = new InviteDoctorFragment();

        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();
    }


    public void loadInviteLaboratory() {
        titleText.setText("Invite to Laboratory");
        clearAboutText();

        InviteLaboratoryFragment fragment = new InviteLaboratoryFragment();

        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();
    }

    public void loadRequestReport(){
        Log.d(G_TAG, "loadRequestReport");
        titleText.setText("Request Report");
        clearAboutText();

        unSelectDrawerItems();
        drawerItemList.get(4).setSelected(true);
        drawerAdapter.notifyDataSetChanged();

        //
        Doc_Patient_ReqReport_list fragment = new Doc_Patient_ReqReport_list();


        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();
    }


    public void loadMyProfile() {
        Log.d(G_TAG, "Fragment My Profile Setting up");
        titleText.setText("My Profile");
        clearAboutText();

        unSelectDrawerItems();
        drawerItemList.get(0).setSelected(true);
        drawerAdapter.notifyDataSetChanged();
        patient_profile fragment = new patient_profile();

        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();
    }


    public void loadInitial() {
        Log.d(G_TAG, "Initial Fragment Setting up");
        titleText.setText("Senz Health");
        clearAboutText();

        unSelectDrawerItems();

        InitialFragment fragment = new InitialFragment();

        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();
    }


    public void loadPatientList() {
        Log.d(G_TAG, "Fragment Patient List Setting up");
        titleText.setText("Patient List");

        clearAboutText();

        unSelectDrawerItems();
        if(isDoc){
            drawerItemList.get(3).setSelected(true);
        }else{
            drawerItemList.get(1).setSelected(true);
        }
        drawerAdapter.notifyDataSetChanged();
        PatientList fragment = new PatientList();

        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();
    }

    public void loadDoctorList() {
        Log.d(G_TAG, "Fragment Doctor List Setting up");
        titleText.setText("Doctor List");
        clearAboutText();

        unSelectDrawerItems();
        drawerItemList.get(1).setSelected(true);
        drawerAdapter.notifyDataSetChanged();

        DoctorListFragment fragment = new DoctorListFragment();

        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();
    }
    public void loadLaboratoryList() {
        Log.d(G_TAG, "Fragment Laboratory List Setting up");
        titleText.setText("Laboratory List");
        clearAboutText();

        unSelectDrawerItems();
        drawerItemList.get(2).setSelected(true);
        drawerAdapter.notifyDataSetChanged();
       LaboratoryListFragment fragment= new LaboratoryListFragment();

        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();
    }

    public void loadGetAppoinment() {
        Log.d(G_TAG, "loadGetAppointment");
        titleText.setText("Get An Appoinment");
        clearAboutText();

        unSelectDrawerItems();
        drawerItemList.get(3).setSelected(true);
        drawerAdapter.notifyDataSetChanged();

        patient_doctor_list_GetAppointment fragment = new patient_doctor_list_GetAppointment();


        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();
    }
    public void loadManageReports() {
        Log.d(G_TAG, "patient_doctor_list_ManageReport Setting up");
        titleText.setText("Manage Reports");
        clearAboutText();

        unSelectDrawerItems();
        if(isLab){
            drawerItemList.get(2).setSelected(true);
        }else{
            drawerItemList.get(4).setSelected(true);
        }
        drawerAdapter.notifyDataSetChanged();

        patient_doctor_list_ManageReport fragment = new patient_doctor_list_ManageReport();



        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();

    }
    public void loadprescriptionDetails() {
        Log.d(G_TAG, "Fragment Prescription Details Setting up");
        titleText.setText("Converse on Prescription Details");
        clearAboutText();

        unSelectDrawerItems();
        drawerItemList.get(5).setSelected(true);
        drawerAdapter.notifyDataSetChanged();

        patient_doctor_list_PrescriptionDetails fragment= new patient_doctor_list_PrescriptionDetails();


        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();
    }



    public void loadLaboratoryProfile() {
        Log.d(G_TAG, "Fragment Laboratory Profile Setting up");
        titleText.setText("My Profile");
        clearAboutText();

        unSelectDrawerItems();
        drawerItemList.get(0).setSelected(true);
        drawerAdapter.notifyDataSetChanged();

        LaboratoryProfileFragment fragment = new LaboratoryProfileFragment();


        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();
    }

    public void loadTimeSchedule() {
        Log.d(G_TAG, "Fragment Time Schedule Setting up");
        titleText.setText("Time Schedule");
        clearAboutText();

        unSelectDrawerItems();
        drawerItemList.get(1).setSelected(true);
        drawerAdapter.notifyDataSetChanged();
        DoctorTimeScheduleFragment fragment = new DoctorTimeScheduleFragment();

        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();
    }
    public void loadDocPrescriptionDetails() {
        Log.d(G_TAG, "Fragment Prescription Details Setting up");
        titleText.setText("Prescription Details");
        clearAboutText();

        unSelectDrawerItems();
        drawerItemList.get(2).setSelected(true);
        drawerAdapter.notifyDataSetChanged();
        DoctorPresciptionDetailsFragment fragment= new DoctorPresciptionDetailsFragment();

        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();
    }
    private void loadChangeRole() {
        drawerItemList.clear();
        boolean drlistisEmpty = drawerItemList.isEmpty();
        Log.d(G_TAG, "Drawerlist is empty :"+drlistisEmpty);
        Log.d(G_TAG, "loadChangeRole()");
        titleText.setText("Change Role");
        selectAboutText();

        drawerLayout.closeDrawer(drawerContainer);
        unSelectDrawerItems();
        drawerAdapter.notifyDataSetChanged();

        Log.d(G_TAG,"Getting Shared Preferences");
        SharedPreferences sharedPreferences = getSharedPreferences("MyRole", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Log.d(G_TAG,"Remove Shared Preference Values");
        editor.clear();
        editor.commit();

        Log.d(G_TAG,"Creating Intent");
        Intent intent=new Intent(this,ActivityChooseRole.class);
        startActivity(intent);
        Log.d(G_TAG,"Finishing drawer activity");
        DrawerActivity.this.finish();

        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        /*FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main,activity);
        transaction.commit();*/
    }

    private void loadAbout() {
        titleText.setText("About");
        selectAboutText();

        drawerLayout.closeDrawer(drawerContainer);
        unSelectDrawerItems();
        drawerAdapter.notifyDataSetChanged();

        AboutFragment fragment = new AboutFragment();

        // fragment transitions
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main, fragment);
        transaction.commit();
    }

    private void unSelectDrawerItems() {
        //  reset content in drawer list
        for (DrawerItem drawerItem : drawerItemList) {
            drawerItem.setSelected(false);
        }
    }

    private void selectAboutText() {
        aboutText.setTextColor(Color.parseColor("#18a351"));
        aboutText.setTypeface(typeface, Typeface.BOLD);

    }

    private void clearAboutText() {
        aboutText.setTextColor(Color.parseColor("#125688"));
        aboutText.setTypeface(typeface, Typeface.BOLD);
    }

}
